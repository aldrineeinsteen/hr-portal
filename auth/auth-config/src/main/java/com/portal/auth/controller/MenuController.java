package com.portal.auth.controller;

import com.portal.auth.model.ERole;
import com.portal.auth.model.Menu;
import com.portal.auth.model.Role;
import com.portal.auth.model.User;
import com.portal.auth.repository.MenuRepository;
import com.portal.auth.repository.RoleRepository;
import com.portal.auth.utils.AuthUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class MenuController {

    private static final Logger logger = LoggerFactory.getLogger(MenuController.class);

    private MenuRepository menuRepository;

    private RoleRepository roleRepository;

    private AuthUtils authUtils;

    @GetMapping("/api/menu")
    public ResponseEntity<?> getMenu() {

        User user = authUtils.getCurrentUser()
                .orElseThrow(
                        () -> new UsernameNotFoundException("Session not found.")
                );
        logger.debug("Retrieving menu for user: {}", user.toString());
        Set<ERole> currentRoles = user.getRoles().stream().map(
                        Role::getName
                )
                .collect(Collectors.toSet());
        Set<Menu> menus = menuRepository.getMenusByAccessibleByIn(
                roleRepository.findRolesByNameIn(currentRoles)
        );
        menus = menus.stream()
                .sorted(
                        Comparator.comparing(Menu::getName)
                ).collect(Collectors.toCollection(LinkedHashSet::new));
        return ResponseEntity.ok(
                menus
        );
    }
}
