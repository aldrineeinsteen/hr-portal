package com.portal.auth.configuration;

import com.portal.auth.model.ERole;
import com.portal.auth.model.Menu;
import com.portal.auth.model.Role;
import com.portal.auth.model.User;
import com.portal.auth.repository.MenuRepository;
import com.portal.auth.repository.RoleRepository;
import com.portal.auth.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
@AllArgsConstructor
public class DataSetUpCLR implements CommandLineRunner {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    private final MenuRepository menuRepository;

    @Override
    public void run(String... args) {
        Role roleUser = insertRoleIfNotExists(roleRepository, ERole.ROLE_USER);

        Role roleAdmin = insertRoleIfNotExists(roleRepository, ERole.ROLE_ADMIN);

        Role roleModerator = insertRoleIfNotExists(roleRepository, ERole.ROLE_MODERATOR);

        insertUserIfNotExists(userRepository, "admin", "admin@portal.com", passwordEncoder, "Admin@123", new Role[]{roleUser, roleAdmin, roleModerator});

        insertUserIfNotExists(userRepository, "user", "user@portal.com", passwordEncoder, "User@123", new Role[]{roleUser});

        Menu homeMenu = insertMenuIfNotExists(
                menuRepository,
                "My Profile",
                "/me",
                null,
                Arrays.stream(new Role[]{roleUser, roleAdmin, roleModerator}).collect(Collectors.toSet()),
                1
        );

        Menu aboutMenu = insertMenuIfNotExists(
                menuRepository,
                "Manage Users",
                "/manage",
                null,
                Arrays.stream(new Role[]{roleAdmin}).collect(Collectors.toSet()),
                3
        );


        Menu searchMenu = insertMenuIfNotExists(
                menuRepository,
                "Search",
                "/search",
                null,
                Arrays.stream(new Role[]{roleAdmin, roleModerator}).collect(Collectors.toSet()),
                2
        );
    }

    private void insertUserIfNotExists(UserRepository userRepository, String user, String email, PasswordEncoder passwordEncoder, String rawPassword, Role[] roles) {
        Optional<User> queriedUser2 = userRepository.findUserByUsername(user);
        if(queriedUser2.isEmpty()) {
            User userObj = new User();
            userObj.setUsername(user);
            userObj.setEmail(email);
            userObj.setPassword(passwordEncoder.encode(rawPassword));
            Set<Role> roleSet = Arrays.stream(roles).collect(Collectors.toSet());
            userObj.setRoles(roleSet);

            userRepository.saveAndFlush(userObj);
        }
    }

    private Role insertRoleIfNotExists(RoleRepository roleRepository, ERole role) {
        Optional<Role> queriedRole = roleRepository.findRoleByName(role);
        if(queriedRole.isEmpty()) {
            Role roleUserObj = new Role();
            roleUserObj.setName(role);

            return roleRepository.saveAndFlush(roleUserObj);
        }
        return queriedRole.get();
    }

    private Menu insertMenuIfNotExists(
            MenuRepository menuRepository,
            String name,
            String url,
            Menu parent,
            Set<Role> roles,
            int position
    ) {
        Optional<Menu> queriedMenu = menuRepository.findMenuByName(name);
        if(queriedMenu.isEmpty()) {
            Menu menuObj = new Menu();
            menuObj.setName(name);
            menuObj.setOrderBy(position);
            menuObj.setAccessibleBy(roles);
            if(null != url)
                menuObj.setUrl(url);
            if(null != parent)
                menuObj.setParent(parent);

            return menuRepository.saveAndFlush(menuObj);
        }
        return queriedMenu.get();
    }
}
