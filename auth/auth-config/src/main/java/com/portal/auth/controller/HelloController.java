package com.portal.auth.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("dev")
public class HelloController {

    @GetMapping("/api/hello")
    public ResponseEntity<?> hello() {
        return ResponseEntity.ok(
                "Hello world"
        );
    }

}
