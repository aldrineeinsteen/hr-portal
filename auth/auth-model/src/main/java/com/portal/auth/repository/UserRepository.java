package com.portal.auth.repository;

import com.portal.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findUserByUsername(String username);

    Boolean existsUserByEmail(String email);

    Boolean existsUserByUsername(String username);
}
