package com.portal.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class Menu extends BaseEntity{

    @NotBlank
    private String name;

    private String url;

    private int orderBy;

    @OneToOne
    private Menu parent;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "menus_roles",
            joinColumns = @JoinColumn(name = "menu_uuid"),
            inverseJoinColumns = @JoinColumn(name = "role_uuid")
    )
    private Set<Role> accessibleBy;
}
