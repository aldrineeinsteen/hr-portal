package com.portal.auth.repository;

import com.portal.auth.model.Menu;
import com.portal.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface MenuRepository extends JpaRepository<Menu, UUID> {

    Set<Menu> getMenusByAccessibleByIn(Set<Role> accessibleBy);

    Optional<Menu> findMenuByName(String name);
}
