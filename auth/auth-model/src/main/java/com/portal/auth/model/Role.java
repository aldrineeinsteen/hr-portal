package com.portal.auth.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Role extends BaseEntity{

    private ERole name;
}
