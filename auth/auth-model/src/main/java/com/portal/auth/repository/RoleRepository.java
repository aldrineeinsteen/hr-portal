package com.portal.auth.repository;

import com.portal.auth.model.ERole;
import com.portal.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {

    Optional<Role> findRoleByName(ERole name);

    Set<Role> findRolesByNameIn(Set<ERole> roles);
}
