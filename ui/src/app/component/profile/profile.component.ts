import {Component, OnInit} from '@angular/core';

import {Login} from "./login";
import {AuthService} from "../../_service/auth/auth.service";
import {TokenService} from "../../_service/token/token.service";

import {faEye, faEyeSlash, faKey, faUserAstronaut} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  showPassword = false;

  faEyeSlash = faEyeSlash
  faEye = faEye
  faKey = faKey
  faUserAstronaut = faUserAstronaut

  loginData = new Login;

  constructor(public authService: AuthService, public tokenService: TokenService) {
  }

  ngOnInit(): void {
    if(this.tokenService.get()) {
      this.authService.setAuthenticated(true);
    }
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  //login
  login() {
    this.authService.login(this.loginData).subscribe(
      {
        next: data => {
          this.tokenService.save(data.token);

          this.authService.setAuthenticated(true);
        }
      }
    );
    this.authService.setAuthenticated(false);
    this.loginData = new Login();
  }

  logout() {
    this.authService.logout()
  }
}
