import {Component, OnInit} from '@angular/core';
import {TokenService} from "../../../_service/token/token.service";
import {AuthService} from "../../../_service/auth/auth.service";
import {MyProfile} from "../my-profile";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  PROFILE_URL = "http://localhost:8080/api/profile"

  myProfile = new MyProfile();

  constructor(public tokenService: TokenService, public authService: AuthService, private http: HttpClient) {
    if (this.authService.isAuthenticated())
      this.getProfile();
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated())
      this.getProfile();
  }

  save(myProfileData: MyProfile) {
    this.http.post<MyProfile>(
      this.PROFILE_URL,
      myProfileData,
      {
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization': "Bearer ".concat(<string>this.tokenService.get())
          })
      }
    ).subscribe(
      data => {
        this.myProfile = data;
      }
    )
  }

  getProfile() {
    this.http.get<MyProfile>(
      this.PROFILE_URL,
      {
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization': "Bearer ".concat(<string>this.tokenService.get())
          })
      })
      .subscribe(
        (data: MyProfile) => {
          this.myProfile = <MyProfile>data;
        }
      );
  }
}
