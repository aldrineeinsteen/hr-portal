import {SkillList} from "./skill-list";

export class MyProfile {
  firstName: String;
  lastName: String;
  middleName: String | undefined;
  address: String | undefined;
  dateOfBirth: Date | undefined;
  summary: String | undefined;
  skillLists: SkillList[] | undefined;
  currentPosition: String | undefined;
  location: String | undefined;
  currentOrganisation: String | undefined;

  constructor() {
    this.firstName = "Jane";
    this.lastName = "Doe";
  }
}
