import {Menu} from "./menu";

export class MenuCollection {
  name: String;
  menus: [Menu] | undefined;

  constructor() {
    this.name = "";
    this.menus = [new Menu()]
  }
}
