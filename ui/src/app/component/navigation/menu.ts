export class Menu {
  name: String;
  url: String | undefined;
  parent: Menu | undefined;
  orderBy: number;

  constructor() {
    this.name = "";
    this.orderBy = 0;
  }
}
