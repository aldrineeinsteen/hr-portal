import {Component, OnInit} from '@angular/core';
import {faCoffee} from '@fortawesome/free-solid-svg-icons';

import {AuthService} from "../../_service/auth/auth.service";
import {MenuService} from "../../_service/menu/menu.service";
import {TokenService} from "../../_service/token/token.service";
import {Menu} from "./menu";
import {HttpClient, HttpHeaders} from "@angular/common/http";


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})


export class NavigationComponent implements OnInit {

  MENU_API = 'http://localhost:8080/api/menu';

  faCoffee = faCoffee;
  menus: any;
  httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
      })
  };

  constructor(
    public authService: AuthService,
    public menuService: MenuService,
    public tokenService: TokenService,
    private http: HttpClient
  ) {
    this.menus = null;
  }

  ngOnInit(): void {
    this.menus = null;
  }

  getMenu() {
    var token = this.tokenService.get();
    console.debug("Retrieved token : " + token)
    if (typeof token === "string") {
      this.httpOptions = {
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization': "Bearer ".concat(token)
          })
      };
    }

    if (this.menus == null)
      this.menus = this.http.get<Menu[]>(this.MENU_API, this.httpOptions);
    return this.menus;
    // return this.http.get<Menu[]>(this.MENU_API, this.httpOptions);
  }


}
