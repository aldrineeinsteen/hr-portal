import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ProfileComponent} from './component/profile/profile.component';
import {NavigationComponent} from './component/navigation/navigation.component';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MyProfileComponent} from './component/myprofile/my-profile/my-profile.component';
import { SearchComponent } from './component/search/search.component';
import { ManageUsersComponent } from './component/manage-users/manage-users.component';
import { HeroContentComponent } from './component/hero-content/hero-content.component';
import { PageNotFouncComponent } from './component/page-not-founc/page-not-founc.component';
import {RouterModule} from "@angular/router";

const routes = [
  {path: 'me', component: MyProfileComponent},
  {path: 'manage', component: ManageUsersComponent},
  {path: 'search', component: SearchComponent},
  {path: '', component: HeroContentComponent},
  {path: '**', component: PageNotFouncComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    NavigationComponent,
    MyProfileComponent,
    SearchComponent,
    ManageUsersComponent,
    HeroContentComponent,
    PageNotFouncComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
