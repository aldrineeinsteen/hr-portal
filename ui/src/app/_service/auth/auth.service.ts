import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Login} from "../../component/profile/login";
import {TokenService} from "../token/token.service";

const AUTH_API = 'http://localhost:8080/api/auth';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticated = false;

  constructor(private http: HttpClient, private tokenService: TokenService) { }

  login(loginData: Login): Observable<any> {
    return this.http.post(AUTH_API + "/login" , loginData, httpOptions);
  }

  logout() {
    this.setAuthenticated(false);
    this.tokenService.clear();
  }

  isAuthenticated(): boolean{
    return this.authenticated;
  }

  setAuthenticated(val: boolean) {
    this.authenticated = val;
  }
}
