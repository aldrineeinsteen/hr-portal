import {Injectable} from '@angular/core';

const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  clear(): void {
    window.sessionStorage.clear();
  }

  public save(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    // localStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
    // localStorage.setItem(TOKEN_KEY, token);
  }

  public get(): string | null {
    var token = window.sessionStorage.getItem(TOKEN_KEY);
    // var token = localStorage.getItem(TOKEN_KEY);
    console.log("Token: " + token)
    if(token == null)
      return ""
    return token;
  }
}
