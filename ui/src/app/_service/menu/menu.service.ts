import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Menu} from "../../component/navigation/menu";
import {TokenService} from "../token/token.service";

const MENU_API = 'http://localhost:8080/api/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
      })
  };

  // httpOptions = {
  //   headers: new HttpHeaders(
  //     {
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer ' + this.tokenService.get()
  //     })
  // };

  menus = new Menu();

  constructor(private http: HttpClient, private tokenService: TokenService) {
    this.httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': "Bearer " + this.tokenService.get()
        })
    };
  }

  getMenu(): Observable<Menu[]> {
    console.log(this.httpOptions)
    return this.http.get<Menu[]>(MENU_API, this.httpOptions);
  }
}
