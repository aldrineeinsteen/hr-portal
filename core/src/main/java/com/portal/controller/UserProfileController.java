package com.portal.controller;

import com.portal.auth.model.User;
import com.portal.auth.repository.UserRepository;
import com.portal.auth.utils.AuthUtils;
import com.portal.model.ESkillLevel;
import com.portal.model.Profile;
import com.portal.model.Skill;
import com.portal.model.SkillProfile;
import com.portal.repository.ProfileRepository;
import com.portal.repository.SkillProfileRepository;
import com.portal.repository.SkillRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@AllArgsConstructor
public class UserProfileController {

    private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    private ProfileRepository profileRepository;

    private SkillRepository skillRepository;

    private SkillProfileRepository skillProfileRepository;

    private UserRepository userRepository;

    private AuthUtils authUtils;

    @GetMapping("/api/profile")
    Profile getMyProfile() {
//        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        System.out.println("Username: " + userDetails.getUsername());
//        Optional<User> userByUsername = userRepository.findUserByUsername(
//                userDetails.getUsername()
//        );
        Optional<User> userByUsername = authUtils.getCurrentUser();
        Optional<Profile> profileByUser = profileRepository.findProfileByUser(userByUsername.orElseThrow(() -> new RuntimeException("User not found")));

        return profileByUser.orElse(getDummyProfile());
    }

    @PostMapping("/api/profile")
    Profile saveMyProfile(
            @RequestBody Profile profile
    ) {

        logger.error("Profile object({}): {}",
                profile.getUuid(),
                profile);
        profile.setUser(authUtils.getCurrentUser().orElseThrow(
                () -> new UsernameNotFoundException("Illegal session.")
        ));

        Set<SkillProfile> toUpsert = new HashSet<>();
        profile.getSkillList().forEach(
                x -> {
                    Skill obj;
                    if (x.getSkill().getUuid() == null) {
                        skillRepository.save(
                                new Skill(x.getSkill().getName())
                        );
                    }
                    obj = skillRepository
                            .findSkillByName(x.getSkill().getName())
                            .orElseGet(() ->
                                    skillRepository.save(
                                            new Skill(x.getSkill().getName())
                                    )
                            );
                    SkillProfile skillProfileObj = skillProfileRepository
                            .findSkillProfileBySkillAndLevel(obj, x.getLevel())
                            .orElseGet(() -> skillProfileRepository.save(
                                            new SkillProfile(obj, x.getLevel())
                                    )
                            );
                    toUpsert.add(skillProfileObj);
                }
        );
        profile.setSkillList(toUpsert);

        return profileRepository.save(profile);
    }

    Profile getDummyProfile() {
        Profile dummy = new Profile();
        dummy.setFirstName("Jane");
        dummy.setLastName("Doe");
        Set<SkillProfile> skillProfileSet = new HashSet<>();
        skillProfileSet.add(
                new SkillProfile(
                        new Skill("Java"),
                        ESkillLevel.EXPERT
                )
        );
        dummy.setSkillList(skillProfileSet);
        return dummy;
    }

    @PostMapping("/api/search")
    Page<Profile> search(
            @RequestBody Set<SkillProfile> skillProfileSet,
            @RequestParam Optional<Integer> page,
            @RequestParam Optional<Integer> size
    ) {

        Pageable pageableReq = PageRequest.of(
                page.orElse(0),
                size.orElse(20),
                Sort.by("lastUpdated").ascending()
        );
        Set<SkillProfile> toQuery = new HashSet<>();
        skillProfileSet.forEach(
                skillProfile -> skillProfileRepository.findSkillProfileBySkillAndLevel(
                        skillRepository.findSkillByName(
                                skillProfile.getSkill().getName()
                        ).orElseGet(() -> skillRepository.save(
                                new Skill(skillProfile.getSkill().getName())
                        )),
                        skillProfile.getLevel()
                ).ifPresent(
                        toQuery::add
                )
        );

        toQuery.forEach(System.out::println);

        return profileRepository.findProfilesBySkillListIn(toQuery, pageableReq);
    }
}
