package com.portal.repository;

import com.portal.auth.model.User;
import com.portal.model.Profile;
import com.portal.model.SkillProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, UUID> {

    Page<Profile> findProfilesBySkillListIn(Set<SkillProfile> profiles, Pageable pageable);

    Optional<Profile> findProfileByUser(User user);

    //Set<Profile> findProfilesBySkillListInAndOrderByLastUpdated(Set<SkillProfile> profiles);
}
