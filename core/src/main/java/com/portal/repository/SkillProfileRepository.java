package com.portal.repository;

import com.portal.model.ESkillLevel;
import com.portal.model.Skill;
import com.portal.model.SkillProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SkillProfileRepository extends JpaRepository<SkillProfile, UUID> {

    Optional<SkillProfile> findSkillProfileBySkillAndLevel(Skill skill, ESkillLevel level);
}
