package com.portal.model;

import com.portal.auth.model.BaseEntity;
import com.portal.auth.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class Profile extends BaseEntity {

    @OneToOne
    //@Column(unique = true)
    private User user;

    private String firstName;

    private String lastName;

    private String middleName;

    private String address;

    private Date dateOfBirth;

    private String summary;

    @ManyToMany
    @JoinTable(
            name = "profiles_skill_list",
            joinColumns = @JoinColumn(name = "profile_uuid"),
            inverseJoinColumns = @JoinColumn(name = "skill_list_uuid")
    )
    private Set<SkillProfile> skillList;

    private String currentPosition;

    private String location;

    private String currentOrganisation;

    private Date lastUpdated;
}
