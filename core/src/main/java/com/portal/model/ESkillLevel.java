package com.portal.model;

public enum ESkillLevel {
    BEGINNER, INTERMEDIATE, EXPERT
}
