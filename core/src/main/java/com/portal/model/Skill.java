package com.portal.model;

import com.portal.auth.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.UniqueConstraint;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
public class Skill extends BaseEntity {

    @Column(unique = true)
    private String name;

    public Skill(String name) {
        this.name = name;
    }
}
