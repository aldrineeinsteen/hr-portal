package com.portal.model;

import com.portal.auth.model.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "SKILL_UUID", "LEVEL" }) })
public class SkillProfile extends BaseEntity {

    @OneToOne
    private Skill skill;

    private ESkillLevel level;

    public SkillProfile(Skill skill, ESkillLevel level) {
        this.skill = skill;
        this.level = level;
    }
}
